/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package test.com.elektrobit.ebrace.targetdata.dlt.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;

import com.elektrobit.ebrace.common.utils.HexStringHelper;
import com.elektrobit.ebrace.targetdata.dlt.internal.DltFileTokenizer;

public class DltFileTokenizerTest
{

    private final static String RAW_DLT_MESSAGE_AS_STR = "35 00 00 20 45 43 55 31 00 37 2E AD 26 01 44 41"
            + " 31 00 44 43 31 00 02 0F 00 00 00 02 00 00 00 00";

    private final static String RAW_INCOMPLETE_DLT_MESSAGE_AS_STR = "35 00 00 20 45 43 55 31 00 37 2E AD 26 01 44 41";

    private final static String STORAGE_HEADER_AS_STR = "44 4C 54 01 86 EE B2 5D D0 0E 0F 00 45 43 55 00";

    private final static String FILE_WITH_STORAGE_HEADER = "temp.dlt";
    private final static String FILE_WITHOUT_STORAGE_HEADER = "temp.dlts";

    private final static int STORAGE_HEADER_LENGTH = 16;
    private final static int RAW_MESSAGE_LENGTH = RAW_DLT_MESSAGE_AS_STR.length() / 3 + 1;
    private final static int RAW_INCOMPLETE_MESSAGE_LENGTH = RAW_INCOMPLETE_DLT_MESSAGE_AS_STR.length() / 3 + 1;
    private final static int NMBR_MULTI_MESSAGES = 1000;

    private static DltFileTokenizer createDltfileTokenizer(String filename, List<String> rawDataAsString, int nmbr)
            throws IOException
    {
        boolean withStorageHeader = true;

        if (filename.endsWith( ".dlts" ))
        {
            withStorageHeader = false;
        }

        FileOutputStream fos = new FileOutputStream( new File( filename ) );
        for (int i = 0; i < nmbr; i++)
        {
            for (String rawItem : rawDataAsString)
            {
                if (withStorageHeader)
                {
                    fos.write( HexStringHelper.hexStringToByteArray( STORAGE_HEADER_AS_STR.replaceAll( "\\s+", "" ) ) );
                }
                fos.write( HexStringHelper.hexStringToByteArray( rawItem.replaceAll( "\\s+", "" ) ) );
            }
        }
        fos.close();

        File f = new File( filename );

        return new DltFileTokenizer( new FileInputStream( f ), f.length(), withStorageHeader );
    }

    @Test
    public void testFileWithOneDltMessageWithoutHeader() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITHOUT_STORAGE_HEADER,
                                                             Collections.singletonList( RAW_DLT_MESSAGE_AS_STR ),
                                                             1 );

        assertTrue( tokenizer.hasNext() );

        assertNotNull( tokenizer.next() );

        assertEquals( RAW_MESSAGE_LENGTH, tokenizer.getFilePointer() );

        assertFalse( tokenizer.hasNext() );

        assertNull( tokenizer.next() );

    }

    @Test
    public void testFileWithMultiDltMessagesWithOutHeader() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITHOUT_STORAGE_HEADER,
                                                             Collections.singletonList( RAW_DLT_MESSAGE_AS_STR ),
                                                             NMBR_MULTI_MESSAGES );

        for (int i = 0; i < NMBR_MULTI_MESSAGES; i++)
        {
            assertTrue( tokenizer.hasNext() );
            assertNotNull( tokenizer.next() );
            assertEquals( RAW_MESSAGE_LENGTH * (i + 1), tokenizer.getFilePointer() );
        }

        assertFalse( tokenizer.hasNext() );
        assertNull( tokenizer.next() );
    }

    @Test
    public void testFileWithOneDltMessageWithHeader() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITH_STORAGE_HEADER,
                                                             Collections.singletonList( RAW_DLT_MESSAGE_AS_STR ),
                                                             1 );

        assertTrue( tokenizer.hasNext() );

        assertNotNull( tokenizer.next() );

        assertEquals( RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH, tokenizer.getFilePointer() );

        assertFalse( tokenizer.hasNext() );

        assertNull( tokenizer.next() );

    }

    @Test
    public void testFileWithMultiDltMessagesWithHeader() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITH_STORAGE_HEADER,
                                                             Collections.singletonList( RAW_DLT_MESSAGE_AS_STR ),
                                                             NMBR_MULTI_MESSAGES );

        for (int i = 0; i < NMBR_MULTI_MESSAGES; i++)
        {
            assertTrue( tokenizer.hasNext() );
            assertNotNull( tokenizer.next() );
            assertEquals( (RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH) * (i + 1), tokenizer.getFilePointer() );
        }

        assertFalse( tokenizer.hasNext() );
        assertNull( tokenizer.next() );
    }

    @Test
    public void testFileWithIncompleteDltMessageAtEndOfFile() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITH_STORAGE_HEADER,
                                                             Arrays.asList( new String[]{RAW_DLT_MESSAGE_AS_STR,
                                                                     RAW_INCOMPLETE_DLT_MESSAGE_AS_STR} ),
                                                             1 );

        assertTrue( tokenizer.hasNext() );
        assertNotNull( tokenizer.next() );
        assertEquals( RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH, tokenizer.getFilePointer() );

        assertTrue( tokenizer.hasNext() );
        assertNull( tokenizer.next() );
        assertEquals( RAW_MESSAGE_LENGTH + RAW_INCOMPLETE_MESSAGE_LENGTH + 2 * STORAGE_HEADER_LENGTH,
                      tokenizer.getFilePointer() );

        assertFalse( tokenizer.hasNext() );
    }

    @Test
    public void testFileWithIncompleteDltMessageInBetween() throws IOException
    {
        DltFileTokenizer tokenizer = createDltfileTokenizer( FILE_WITH_STORAGE_HEADER,
                                                             Arrays.asList( new String[]{RAW_DLT_MESSAGE_AS_STR,
                                                                     RAW_INCOMPLETE_DLT_MESSAGE_AS_STR,
                                                                     RAW_DLT_MESSAGE_AS_STR, RAW_DLT_MESSAGE_AS_STR} ),
                                                             1 );

        assertTrue( tokenizer.hasNext() );
        assertNotNull( tokenizer.next() );
        assertEquals( RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH, tokenizer.getFilePointer() );

        assertTrue( tokenizer.hasNext() );
        assertNotNull( tokenizer.next() );
        assertEquals( 2 * (RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH), tokenizer.getFilePointer() );

        assertTrue( tokenizer.hasNext() );
        assertNotNull( tokenizer.next() );
        assertEquals( 3 * (RAW_MESSAGE_LENGTH + STORAGE_HEADER_LENGTH) + RAW_INCOMPLETE_MESSAGE_LENGTH
                + STORAGE_HEADER_LENGTH, tokenizer.getFilePointer() );

        assertFalse( tokenizer.hasNext() );
        assertNull( tokenizer.next() );
    }

    private static void removeFile(String filename)
    {
        File f = new File( filename );
        if (f.exists())
        {
            f.delete();
        }
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
        removeFile( FILE_WITH_STORAGE_HEADER );
        removeFile( FILE_WITHOUT_STORAGE_HEADER );
    }

}

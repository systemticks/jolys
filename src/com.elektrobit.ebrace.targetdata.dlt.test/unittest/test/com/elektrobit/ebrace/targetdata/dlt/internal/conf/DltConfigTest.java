/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package test.com.elektrobit.ebrace.targetdata.dlt.internal.conf;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.elektrobit.ebrace.targetdata.dlt.internal.conf.DltConfItem;
import com.elektrobit.ebrace.targetdata.dlt.internal.conf.DltConfig;

public class DltConfigTest
{
    private DltConfig dltConf;

    @Before
    public void setup()
    {
        dltConf = new DltConfig();
    }

    @Test
    public void testDoMatchExact()
    {
        DltConfItem c = new DltConfItem();
        c.setAppId( "APP1" );
        c.setCtxId( "CTX1" );

        assertTrue( dltConf.doMatch( c, "APP1", "CTX1" ) );
        assertFalse( dltConf.doMatch( c, "APP1", "CTX2" ) );
        assertFalse( dltConf.doMatch( c, "APP2", "CTX1" ) );
    }

    @Test
    public void testDoMatchWildcardCtx()
    {
        DltConfItem c = new DltConfItem();
        c.setAppId( "APP1" );
        c.setCtxId( "*" );

        assertTrue( dltConf.doMatch( c, "APP1", "CTX1" ) );
        assertTrue( dltConf.doMatch( c, "APP1", "CTX2" ) );
        assertFalse( dltConf.doMatch( c, "APP2", "CTX1" ) );
    }

    @Test
    public void testDoMatchWildcardApp()
    {
        DltConfItem c = new DltConfItem();
        c.setAppId( "*" );
        c.setCtxId( "CTX1" );

        assertTrue( dltConf.doMatch( c, "APP1", "CTX1" ) );
        assertFalse( dltConf.doMatch( c, "APP1", "CTX2" ) );
        assertTrue( dltConf.doMatch( c, "APP2", "CTX1" ) );
    }

    @Test
    public void testDoMatchWildcardAll()
    {
        DltConfItem c = new DltConfItem();
        c.setAppId( "*" );
        c.setCtxId( "*" );

        assertTrue( dltConf.doMatch( c, "APP1", "CTX1" ) );
        assertTrue( dltConf.doMatch( c, "APP1", "CTX2" ) );
        assertTrue( dltConf.doMatch( c, "APP2", "CTX1" ) );
    }

    @Test
    public void testCreateDefaultConfig()
    {
        File d = dltConf.createDefaultConfig();
        assertNotNull( d );

        assertTrue( d.exists() );
    }

    @Test
    public void testInitFilterRulesDefault()
    {
        dltConf.initFilterRules();

        assertTrue( dltConf.isCompliantWithFilterRules( "ANY", "ANY" ) );
    }

    @Test
    public void testInitFilterRulesTestFile01() throws IOException
    {
        FileUtils.copyFile( new File( "testdata/dlt-test01.conf" ), new File( "dlt.conf" ) );

        dltConf.initFilterRules();

        assertTrue( dltConf.isCompliantWithFilterRules( "APP1", "ANY" ) );
        assertFalse( dltConf.isCompliantWithFilterRules( "APP2", "ANY" ) );

        assertTrue( dltConf.isCompliantWithFilterRules( "ANY", "CTX1" ) );
        assertFalse( dltConf.isCompliantWithFilterRules( "ANY", "CTX2" ) );

        assertTrue( dltConf.isCompliantWithFilterRules( "APP2", "CTX2" ) );
        assertFalse( dltConf.isCompliantWithFilterRules( "ANY", "ANY" ) );

    }

    @Test
    public void testInitFilterRulesTestFileEmpty() throws IOException
    {
        FileUtils.copyFile( new File( "testdata/dlt-empty.conf" ), new File( "dlt.conf" ) );

        dltConf.initFilterRules();

        assertTrue( dltConf.isCompliantWithFilterRules( "ANY", "ANY" ) );

    }

    @After
    public void cleanup()
    {
        File f = new File( "dlt.conf" );
        if (f.exists())
        {
            f.delete();
        }
    }

}

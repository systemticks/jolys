/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package de.systemticks.solys.eventhooks.dlt.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Interface1ParserTest
{

    private final static String TEST_STR = "\"daimler.if1verbose: 5503 OFNInput#4:EV:ofnTouchStateChanged(touchState: OFNTouchState(currentTouchState: NO_TOUCH), clientID: 0)\"";
    private final static String TEST_STR_NEW = "\"[daimler.if1verbose] 412 Network#4:EV:networkStatusChanged(networkInfo: NetworkInfo(deviceID: 1283322478, networkSignalStrength: 4, networkName: Vodafone.de, roamingStatus: NO_ROAMING, isNetworkAvailable: 1, isSignalStrengthAvailable: 1, isRegisteredToNetwork: 1, isNetworkNameAvailable: 1)) [unknown:0]\"";

    // seqId = 5503
    // receiver: OFNInput#4
    // msgType = EV/RQ/RP/EX
    // method (with params) : EV:ofnTouchStateChanged ...

    @Test
    public void parseSimpleExample() throws Exception
    {
        IF1Call call = IF1Parser.parseEvent( TEST_STR );
        assertEquals( 5503, call.getSeqId() );
        assertEquals( "OFNInput#4", call.getReceiver() );
        assertEquals( "EV", call.getMsgType() );
        assertEquals( "ofnTouchStateChanged", call.getMethod() );
        assertTrue( call.getParams().containsKey( "touchState" ) );
    }

    @Test
    public void parseSimpleExampleNewFormat() throws Exception
    {
        IF1Call call = IF1Parser.parseEvent( TEST_STR_NEW );
        assertEquals( 412, call.getSeqId() );
        assertEquals( "Network#4", call.getReceiver() );
        assertEquals( "EV", call.getMsgType() );
        assertEquals( "networkStatusChanged", call.getMethod() );
        assertTrue( call.getParams().containsKey( "networkInfo" ) );
        assertEquals( "NetworkInfo", call.getParams().get( "networkInfo" ) );
    }

}

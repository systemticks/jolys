/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package de.systemticks.solys.eventhooks.dlt.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IF1Parser
{

    private final static String REG_EX = "\"\\[?daimler\\.if1verbose\\]?:?\\s+(?<seqId>\\d*)\\s+(?<receiver>[^:]*):(?<msgType>[^:]*):(?<method>.*)\"$";
    private static Pattern pattern = Pattern.compile( REG_EX );

    public static IF1Call parseEvent(String eventString)
    {
        final Matcher matcher = pattern.matcher( eventString );
        IF1Call result = null;
        if (matcher.matches())
        {
            final int seqId = Integer.parseInt( matcher.group( "seqId" ) );
            final String receiver = matcher.group( "receiver" );
            final String msgType = matcher.group( "msgType" );
            final String method = matcher.group( "method" );

            String methodName = method.split( "\\(" )[0];
            String parameterList = method.substring( method.indexOf( "(" ) );

            Map<String, String> parameters = splitParameters( parameterList );
            result = new IF1Call( seqId, receiver, msgType, methodName, parameters );
        }
        return result;
    }

    private static Map<String, String> splitParameters(String parameterList)
    {
        Map<String, String> parameters = new HashMap<>();
        String parent = "";
        for (String parameter : parameterList.split( "[,\\(\\)]+" ))
        {
            String[] keyValue = parameter.split( ":" );
            if (keyValue.length > 1)
            {
                String key = keyValue[0].trim();
                String value = keyValue[1].trim();
                parameters.put( key, value );
            }
            else if (keyValue.length == 1 && keyValue[0].length() > 0)
            {
                parameters.put( keyValue[0], "-" );
            }
        }
        return parameters;
    }

}

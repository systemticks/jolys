package de.systemticks.solys.eventhooks.dlt.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.elektrobit.ebrace.core.targetdata.api.json.JsonEvent;
import com.elektrobit.ebrace.core.targetdata.api.json.JsonEventEdge;
import com.elektrobit.ebrace.core.targetdata.api.json.JsonEventHandler;
import com.elektrobit.ebrace.core.targetdata.api.json.JsonEventValue;
import com.elektrobit.ebsolys.core.targetdata.api.runtime.eventhandling.RuntimeEvent;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import de.systemticks.ebrace.core.eventhook.registry.api.EventHook;

@Component(service = EventHook.class)
public class IF1EventHook implements EventHook
{
    private static final Gson GSON = new Gson();
    private JsonEventHandler jsonEventHandler;

    @Reference
    public void bindJsonService(JsonEventHandler runtimeEventAcceptor)
    {
        this.jsonEventHandler = runtimeEventAcceptor;
    }

    public void unbindJsonService(JsonEventHandler runtimeEventAcceptor)
    {
        this.jsonEventHandler = null;
    }

    @Override
    public void onEvent(RuntimeEvent<?> event)
    {
        if (event.getRuntimeEventChannel().getName().toLowerCase().equals( "trace.dlt.log.ui.if1" ))
        {
            JsonEvent oldEvent = GSON.fromJson( event.getValue().toString(), JsonEvent.class );
            String summaryString = oldEvent.getValue().getDetails().getAsJsonObject().get( "payload" ).getAsJsonObject()
                    .get( "0" ).toString();

            IF1Call call = IF1Parser.parseEvent( summaryString );

            if (call != null)
            {
                jsonEventHandler.handle( new JsonEvent( event.getTimestamp(),
                                                        "com.if1",
                                                        new JsonEventValue( call.getMethod(),
                                                                            GSON.fromJson( GSON.toJson( call ),
                                                                                           JsonElement.class ) ),
                                                        null,
                                                        new JsonEventEdge( "UI.IF1",
                                                                           "Service." + call.getReceiver(),
                                                                           "REQ" ) ) );

            }
            else
            {
                System.out.println( "Ignoring IF1 message: " + summaryString );
            }

            // create JSON Event

        }
    }

}

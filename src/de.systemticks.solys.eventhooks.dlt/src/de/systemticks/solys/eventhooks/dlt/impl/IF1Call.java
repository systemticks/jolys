/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package de.systemticks.solys.eventhooks.dlt.impl;

import java.util.Map;

import lombok.Data;

@Data
public class IF1Call
{
    private final int seqId;
    private final String receiver;
    private final String msgType;
    private final String method;
    private final Map<String, String> params;
}

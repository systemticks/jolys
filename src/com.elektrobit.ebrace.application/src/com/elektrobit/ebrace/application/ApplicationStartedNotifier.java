/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package com.elektrobit.ebrace.application;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.elektrobit.ebrace.dev.usestatlogsannotationloader.api.UseStatLog;
import com.elektrobit.ebrace.dev.usestatlogsannotationloader.api.UseStatLogTypes;
import com.elektrobit.ebsolys.core.targetdata.api.reset.StartupDoneListener;

import lombok.extern.log4j.Log4j;

@Log4j
@Component(immediate = true, enabled = true, property = "event.topics=org/eclipse/e4/ui/LifeCycle/appStartupComplete")
public class ApplicationStartedNotifier implements EventHandler
{

    private volatile Set<StartupDoneListener> listeners = new CopyOnWriteArraySet<>();
    private volatile boolean applicationStarted = false;

    @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.MULTIPLE)
    public void bindStartupDoneListener(StartupDoneListener l)
    {
        if (!applicationStarted)
        {
            listeners.add( l );
        }
        else
        {
            l.onApplicationStarted();
        }
    }

    public void unbindStartupDoneListener(StartupDoneListener l)
    {
        listeners.remove( l );
    }

    @UseStatLog(UseStatLogTypes.APP_STARTUP_FINISHED)
    @Override
    public void handleEvent(Event event)
    {
        applicationStarted = true;
        listeners.forEach( l -> l.onApplicationStarted() );
        log.info( "Number of StartupDoneListener notified " + listeners.size() );
    }

}

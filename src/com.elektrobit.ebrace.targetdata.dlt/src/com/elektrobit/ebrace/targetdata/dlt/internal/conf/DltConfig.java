/*******************************************************************************
 * Copyright (C) 2019 systemticks GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package com.elektrobit.ebrace.targetdata.dlt.internal.conf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.log4j.Log4j;

@Log4j
public class DltConfig
{
    private final String DLT_CONFIG_FILE = "dlt.conf";
    private final String RULE_PATTERN = "(?<appId>\\w{1,4}|\\*)\\.(?<ctxId>\\w{1,4}|\\*)$";
    private final String FILTER_ALL_PATTERN = "*.*";
    private final String WILDCARD = "*";
    private final Pattern pattern;
    private boolean applyFilterRules;
    private final List<DltConfItem> configurationItems;

    public DltConfig()
    {
        pattern = Pattern.compile( RULE_PATTERN );
        applyFilterRules = true;
        configurationItems = new ArrayList<DltConfItem>();
    }

    public File createDefaultConfig()
    {
        File confFile = new File( DLT_CONFIG_FILE );

        try
        {
            FileWriter fw = new FileWriter( confFile );
            fw.write( "# Default Configuration for filtering messages. based on Application ID and Context ID\n" );
            fw.write( "# e.g. APP1.CTX1\n" );
            fw.write( "# e.g. APP1.*\n" );
            fw.write( "# e.g. *.CTX1\n" );
            fw.write( "# e.g. *.*\n" );
            fw.write( "*.*\n" );
            fw.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            log.error( "Default configuration file for filtering could not be created." );
        }

        return confFile;
    }

    public boolean doMatch(DltConfItem cItem, String appId, String ctxId)
    {
        return (cItem.getAppId().equals( WILDCARD ) || cItem.getAppId().equals( appId ))
                && (cItem.getCtxId().equals( WILDCARD ) || cItem.getCtxId().equals( ctxId ));
    }

    public boolean isCompliantWithFilterRules(String appId, String ctxId)
    {
        return !applyFilterRules || configurationItems.stream().anyMatch( c -> doMatch( c, appId, ctxId ) );
    }

    public void initFilterRules()
    {
        File file = new File( DLT_CONFIG_FILE );

        if (!file.exists())
        {
            file = createDefaultConfig();
        }

        if (file == null || !file.exists())
        {
            applyFilterRules = false;
            return;
        }

        else
        {
            try
            {
                BufferedReader reader = new BufferedReader( new FileReader( file ) );
                String line;
                while ((line = reader.readLine()) != null)
                {
                    if (line.contentEquals( FILTER_ALL_PATTERN ))
                    {
                        applyFilterRules = false;
                        break;
                    }

                    // ignore comments and empty lines
                    if (!line.startsWith( "#" ) && line.length() > 0)
                    {
                        Matcher m = pattern.matcher( line );
                        if (m.matches())
                        {
                            DltConfItem cItem = new DltConfItem();
                            cItem.setAppId( m.group( "appId" ) );
                            cItem.setCtxId( m.group( "ctxId" ) );
                            configurationItems.add( cItem );
                        }
                    }
                }
                reader.close();

                if (applyFilterRules && configurationItems.size() == 0)
                {
                    applyFilterRules = false;
                }
            }
            catch (IOException e)
            {
                log.error( "Default configuration file for filtering could not be opened." );
            }
        }
    }

}

/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package com.elektrobit.ebrace.targetdata.dlt.internal;

import java.io.IOException;

import com.elektrobit.ebrace.targetadapter.communicator.api.BytesFromStreamReader;
import com.elektrobit.ebrace.targetadapter.communicator.api.MessageReader;
import com.elektrobit.ebrace.targetdata.dlt.internal.connection.DltChannelFromLogInfoCreator;
import com.elektrobit.ebrace.targetdata.dlt.newapiimpl.DltMessageServiceImpl;

public class DltMessageWithStorageHeaderParser implements MessageReader<DltMessage>
{
    final static int STANDARD_HEADER_SIZE = 4;
    final static int EXTENDED_HEADER_SIZE = 10;
    final static byte[] expectedPattern = new byte[]{68, 76, 84, 1};

    private final DltMessageServiceImpl withoutStorageHeaderParser;

    public DltMessageWithStorageHeaderParser(DltChannelFromLogInfoCreator channelFromLogInfoCreator)
    {
        withoutStorageHeaderParser = new DltMessageServiceImpl( channelFromLogInfoCreator );
    }

    @Override
    public DltMessage readNextMessage(BytesFromStreamReader bytesReader)
    {
        try
        {
            return readNextMessageUnChecked( bytesReader );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public DltMessage readNextMessageUnChecked(BytesFromStreamReader bytesReader)
            throws IOException, DltMessageParseException
    {
        return withoutStorageHeaderParser.readNextMessage( bytesReader );
    }

}

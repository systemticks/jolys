/*******************************************************************************
 * Copyright (C) 2018 Elektrobit Automotive GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package com.elektrobit.ebrace.targetdata.dlt.internal;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.elektrobit.ebrace.chronograph.api.TimestampProvider;
import com.elektrobit.ebrace.core.importerregistry.api.AbstractImporter;
import com.elektrobit.ebrace.targetadapter.communicator.api.MessageReader;
import com.elektrobit.ebrace.targetadapter.communicator.services.ProtocolMessageDispatcher;
import com.elektrobit.ebsolys.core.targetdata.api.runtime.eventhandling.RuntimeEventAcceptor;
import com.elektrobit.ebsolys.core.targetdata.api.timemarker.TimeMarkerManager;

import lombok.extern.log4j.Log4j;

@Log4j
public abstract class DltAbstractImporter extends AbstractImporter
{
    protected TimestampProvider timestampProvider;
    protected TimeMarkerManager timeMarkerManager;
    protected ProtocolMessageDispatcher protocolMessageDispatcher;
    protected RuntimeEventAcceptor runtimeEventAcceptor;

    @Override
    public void processFileContent(File file) throws IOException
    {

        final long fileLength = file.length();
        final int PROGRESS_UPDATE_FREQUENCY = 1000;
        int msgCount = 0;

        long t1 = System.currentTimeMillis();
        DltFileTokenizer tokenizer;

        if (file.getName().endsWith( ".dlts" ))
        {
            tokenizer = new DltFileTokenizer( new FileInputStream( file ), file.length(), false );
        }
        else
        {
            tokenizer = new DltFileTokenizer( new FileInputStream( file ), file.length(), true );
        }

        MessageReader<DltMessage> parser = getMessageParser();

        DltMessageProcessor messageProcessor = new DltMessageProcessor( file.getName(),
                                                                        fileLength,
                                                                        timestampProvider,
                                                                        timeMarkerManager,
                                                                        protocolMessageDispatcher,
                                                                        runtimeEventAcceptor );

        while (tokenizer.hasNext() && !isImportCanceled())
        {
            byte[] rawMessage = tokenizer.next();
            if (rawMessage != null)
            {
                BytesFromStreamReaderImpl reader = new BytesFromStreamReaderImpl( new ByteArrayInputStream( rawMessage ) );
                DltMessage dltMsg = parser.readNextMessage( reader );
                messageProcessor.processMessage( dltMsg );
                if (++msgCount % PROGRESS_UPDATE_FREQUENCY == 0)
                {
                    postProgress( tokenizer.getFilePointer(), fileLength );
                }
            }
        }

        long t2 = System.currentTimeMillis();
        log.info( "Loading and processing file " + file.getName() + " took " + (t2 - t1) + " ms" );

    }

    protected abstract MessageReader<DltMessage> getMessageParser();

    @Override
    protected abstract long getMaximumTraceFileSizeInMB();

    @Override
    public abstract String getSupportedFileExtension();

    @Override
    public abstract String getSupportedFileTypeName();
}

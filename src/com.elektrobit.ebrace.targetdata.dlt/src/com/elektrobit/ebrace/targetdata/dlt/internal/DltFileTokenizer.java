/*******************************************************************************
 * Copyright (C) 2019 systemticks GmbH
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package com.elektrobit.ebrace.targetdata.dlt.internal;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Arrays;
import java.util.Iterator;

import lombok.extern.log4j.Log4j;

@Log4j
public class DltFileTokenizer implements Iterator<byte[]>
{
    private final byte[] DLT_PATTERN = new byte[]{0x44, 0x4C, 0x54, 0x01};
    private final byte[] dltPatternBuffer = new byte[DLT_PATTERN.length];
    private final int REMAINING_STORAGE_HEADER_SIZE = 16 - DLT_PATTERN.length;
    private final int STANDARD_HEADER_SIZE = 4;
    private final int BUFFER_SIZE = 65536;
    byte[] dltMessageBuffer = new byte[BUFFER_SIZE];
    byte[] storageHeaderBuffer = new byte[REMAINING_STORAGE_HEADER_SIZE];

    private final PushbackInputStream inputStream;
    private final boolean withStorageHeader;
    private long filePointer;
    private final long fileLength;
    private final static int EOF = -1;

    public DltFileTokenizer(FileInputStream fis, long _fileLength, boolean _withStorageHeader)
    {
        fileLength = _fileLength;
        filePointer = 0;
        withStorageHeader = _withStorageHeader;
        inputStream = new PushbackInputStream( new BufferedInputStream( fis ), DLT_PATTERN.length );
    }

    private void read(byte[] buffer, int offset, int len) throws IOException
    {
        int effectiveRead = inputStream.read( buffer, offset, len );
        if (!(effectiveRead == EOF))
        {
            filePointer += effectiveRead;
            if (effectiveRead < len)
            {
                log.error( "Could not read all required bytes from stream. Re-try with reading remaining bytes" );
                read( buffer, offset + effectiveRead, len - effectiveRead );
            }
        }
        else
        {
            filePointer = fileLength;
            throw new EOFException();
        }
    }

    private void read(byte[] buffer) throws IOException
    {
        read( buffer, 0, buffer.length );
    }

    private void pushBackAndReadNext(byte[] buffer) throws IOException
    {
        inputStream.unread( buffer );
        filePointer -= buffer.length;
        int effectiveRead = inputStream.read();
        if (!(effectiveRead == EOF))
        {
            filePointer += 1;
        }
        else
        {
            filePointer = fileLength;
            throw new EOFException();
        }
    }

    private void findNextStorageHeader() throws IOException
    {
        boolean storageHeaderFound = false;

        while (!storageHeaderFound)
        {
            read( dltPatternBuffer );

            if (Arrays.equals( dltPatternBuffer, DLT_PATTERN ))
            {
                storageHeaderFound = true;
            }
            else
            {
                pushBackAndReadNext( dltPatternBuffer );
            }
        }
    }

    private byte[] extractNext() throws IOException
    {
        byte[] nextChunk = null;
        int bufPos = 0;
        int len = 0;

        if (filePointer >= fileLength)
        {
            return null;
        }

        if (withStorageHeader)
        {
            findNextStorageHeader();
            read( storageHeaderBuffer );
        }

        read( dltMessageBuffer, bufPos, STANDARD_HEADER_SIZE );
        bufPos += STANDARD_HEADER_SIZE;
        len = (((dltMessageBuffer[bufPos - 2] & 0xff) << 8) | (dltMessageBuffer[bufPos - 1] & 0xff));

        read( dltMessageBuffer, bufPos, len - STANDARD_HEADER_SIZE );

        nextChunk = Arrays.copyOfRange( dltMessageBuffer, 0, len );

        return nextChunk;

    }

    @Override
    public boolean hasNext()
    {
        try
        {
            if (fileLength - filePointer > 0)
            {
                return true;
            }
            else
            {
                inputStream.close();
                return false;
            }
        }
        catch (IOException e)
        {
            log.error( e.getMessage() );
            return false;
        }

    }

    @Override
    public byte[] next()
    {
        try
        {
            return extractNext();
        }
        catch (EOFException e)
        {
            log.error( "EOF reached: Last Dlt Message is incomplete" );
            return null;
        }
        catch (IOException e)
        {
            log.error( "Error while reading from file: " + e.getMessage() );
            return null;
        }
    }

    public long getFilePointer()
    {
        return filePointer;
    }

}
